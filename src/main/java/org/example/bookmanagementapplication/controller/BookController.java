package org.example.bookmanagementapplication.controller;

import lombok.RequiredArgsConstructor;
import org.example.bookmanagementapplication.dto.BookRequestDto;
import org.example.bookmanagementapplication.dto.BookResponseDto;
import org.example.bookmanagementapplication.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public ResponseEntity<List<BookResponseDto>> getAllBooks() {
        return ResponseEntity
                .ok(
                        this.bookService.getBooks()
                );
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<BookResponseDto> getBookById(@PathVariable Long id) {
        return ResponseEntity
                .ok(
                        this.bookService.getBook(id)
                );
    }


    @PostMapping
    public ResponseEntity<Void> createBook(@RequestBody BookRequestDto bookRequestDto) {
        long id = this.bookService.create(bookRequestDto);
        return ResponseEntity
                .created(
                        URI.create("/api/v1/books/" + id)
                )
                .build();
    }


    @PutMapping(path = "/{id}")
    public ResponseEntity<BookResponseDto> updateBook(@PathVariable Long id, @RequestBody BookRequestDto bookRequestDto) {
        BookResponseDto bookResponseDto = this.bookService.updateBook(id, bookRequestDto);
        return ResponseEntity
                .ok(
                        bookResponseDto
                );
    }


    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteBook(@PathVariable Long id) {
        this.bookService.deleteBook(id);
        return ResponseEntity
                .ok()
                .build();
    }
}
