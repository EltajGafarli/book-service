package org.example.bookmanagementapplication.mapper;

import org.example.bookmanagementapplication.dto.BookRequestDto;
import org.example.bookmanagementapplication.dto.BookResponseDto;
import org.example.bookmanagementapplication.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface BookMapper {
    Book requestToBook(BookRequestDto bookRequestDto);
    BookResponseDto bookToResponse(Book book);
}
