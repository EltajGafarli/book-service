package org.example.bookmanagementapplication.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Objects;

@Entity
@Table(name = "books")

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Book extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private String author;
    private int pageCount;

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                title,
                author,
                pageCount
        );
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Book book = (Book) obj;

        return Objects
                .deepEquals(
                        this.id, book.id) &&
                Objects
                        .deepEquals(this.author, book.author) &&
                Objects
                        .deepEquals(this.pageCount, book.pageCount) &&
                Objects
                        .deepEquals(this.title, book.title);
    }
}
