package org.example.bookmanagementapplication.service;

import org.example.bookmanagementapplication.dto.BookRequestDto;
import org.example.bookmanagementapplication.dto.BookResponseDto;

import java.util.List;

public interface BookService {
    long create(BookRequestDto bookRequestDto);
    BookResponseDto updateBook(long bookId, BookRequestDto bookRequestDto);
    void deleteBook(long bookId);
    List<BookResponseDto> getBooks();
    BookResponseDto getBook(long bookId);
}
