package org.example.bookmanagementapplication.service.impl;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.example.bookmanagementapplication.dto.BookRequestDto;
import org.example.bookmanagementapplication.dto.BookResponseDto;
import org.example.bookmanagementapplication.mapper.BookMapper;
import org.example.bookmanagementapplication.model.Book;
import org.example.bookmanagementapplication.repository.BookRepository;
import org.example.bookmanagementapplication.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import jakarta.transaction.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;


    @Override
    public long create(BookRequestDto bookRequestDto) {
        Book book = bookMapper.requestToBook(bookRequestDto);
        Book savedBook = bookRepository.save(book);
        return savedBook.getId();
    }

    @Override
    public BookResponseDto updateBook(long bookId, BookRequestDto bookRequestDto) {
        Book book = bookRepository.findById(bookId).orElseThrow();

        if(bookRequestDto.getAuthor() != null) {
            book.setAuthor(
                    bookRequestDto.getAuthor()
            );
        }

        if(bookRequestDto.getTitle() != null) {
            book.setTitle(
                    bookRequestDto.getTitle()
            );
        }

        book.setPageCount(
                bookRequestDto.getPageCount()
        );

        Book savedBook = bookRepository.save(book);

        return bookMapper.bookToResponse(savedBook);


    }

    @Override
    public void deleteBook(long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Override
    public List<BookResponseDto> getBooks() {
        return bookRepository
                .findAll()
                .stream()
                .map(bookMapper::bookToResponse)
                .toList();
    }

    @Override
    public BookResponseDto getBook(long bookId) {
        return  bookRepository
                .findById(bookId)
                .map(bookMapper::bookToResponse)
                .orElseThrow();
    }

}
